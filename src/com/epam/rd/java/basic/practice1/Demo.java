package com.epam.rd.java.basic.practice1;

public class Demo
{
    public static void main(String[] args) {

        Part1.main(new String[] {});
        System.out.println("First part");

        Part2.main(new String[]{"4","3","2"});
        System.out.println("Second part");

        Part3.main(new String[]{"1 plus 5 = 6"});
        System.out.println("Third part");

        Part4.main(new String[]{"6","9"});
        System.out.println("Fourth part");

        Part5.main(new String[]{"236"});
        System.out.println("Fifth part");

        Part6.main(new String[]{"20"});
        System.out.println("Sixth part");

        Part7.main(new String[]{"G","13","ZC"});
        System.out.println("Seventh part");
    }
}
