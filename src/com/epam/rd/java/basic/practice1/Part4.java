package com.epam.rd.java.basic.practice1;

public class Part4
{
    public static void main(String[] args)
    {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        System.out.println(delitel(a,b));
    }
    public static int delitel(int a, int b)
    {
        int d = 0;
        while (b != 0 && a != 0) {
            if (a > b)
            {
                a %= b;
            }
            else
            {
                b %= a;
            }
            d = a + b;
        }
        return d;
    }
}
