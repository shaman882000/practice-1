package com.epam.rd.java.basic.practice1;

public class Part6
{
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]); // Length of array
        int[] array = new int[N]; // Array of simple numbers
        int nextNumber = 2; // Testing number
        int counter = 0; // Counter of filling element of array
        boolean isSimple;
        while (counter < N) {
            int i = 0;
            isSimple = true;
            while (isSimple && i < counter) {
                if (nextNumber % array[i] == 0) {
                    isSimple = false;
                }
                i++;
            }
            if (isSimple) {
                array[counter] = nextNumber;
                counter++;
            }
                nextNumber++;

        }
        for(int i=0;i<array.length;i++){
            System.out.print(array[i]+" ");
        }

    }
}
