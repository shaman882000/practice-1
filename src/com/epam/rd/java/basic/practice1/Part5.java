package com.epam.rd.java.basic.practice1;

public class Part5
{
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int sum = 0;
        System.out.println("Number:" + a);
        while (a != 0) {
            sum += a % 10;
            a /= 10;
        }
        System.out.println("Sum"+sum);
    }
}
